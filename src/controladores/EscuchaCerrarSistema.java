/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author dvillanueva
 */
public class EscuchaCerrarSistema extends WindowAdapter{
    CerrarVentana quienLlama;
    
    public EscuchaCerrarSistema(CerrarVentana llamante){
        quienLlama = llamante;
    }
    /**
     *
     * @param e evento 
     */
    public void windowClosing(WindowEvent e) {
        quienLlama.cerrarVentana();
    }  
}
