package controladores;

/**
 *
 * @author Hector Alvarez
 */
public interface AccionBotonFormulario {
    public void aceptarAccion();
    public void cancelarAccion();
    public void estatusAccion(int estatus);
}