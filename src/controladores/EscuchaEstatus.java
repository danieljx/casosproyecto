/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 
 */
public class EscuchaEstatus implements ActionListener {
    AccionBotonFormulario quienLlama;
    int estatus;
    public EscuchaEstatus(int estatus,AccionBotonFormulario quienLlama){
        this.quienLlama = quienLlama;
        this.estatus = estatus;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        quienLlama.estatusAccion(estatus);
    }
}
