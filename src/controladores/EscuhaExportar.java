/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author dvillanueva
 */
public class EscuhaExportar implements ActionListener {
    AccionBoton quienLlama;
    public EscuhaExportar(AccionBoton quienLlama) {
        this.quienLlama = quienLlama;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        this.quienLlama.exportarAccion();
    }
}
