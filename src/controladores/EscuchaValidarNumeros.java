/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author Anthony
 */
public class EscuchaValidarNumeros implements KeyListener {
    AccionValidarCampos quienLlama;
    
    public EscuchaValidarNumeros(AccionValidarCampos llamante){
        quienLlama = llamante;
    }
    @Override
    public void keyTyped(KeyEvent ke) {
        char c = ke.getKeyChar();
          quienLlama.numerosAccion(Character.isDigit(c));
          if(!Character.isDigit(c)) {
              ke.consume();
          }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
    
}
