/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author Anthony
 */
public class EscuchaSalir implements MouseListener {
    CerrarVentana quienLlama;
    
    public EscuchaSalir(CerrarVentana llamante){
        quienLlama = llamante;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        quienLlama.cerrarVentana();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
}
