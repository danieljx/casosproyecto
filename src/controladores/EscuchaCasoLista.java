/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import vistas.Contenido;

/**
 *
 * @author dvillanueva
 */
public class EscuchaCasoLista implements ActionListener {
    Contenido contenido;
    public EscuchaCasoLista(Contenido contenido) {
        this.contenido = contenido;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        this.contenido.abrirListaCasos();
    }
}
