/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import Componentes.ConexionBD;
import static SQL.MensajesDeError.errorSQL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author dvillanueva
 */
public class Empresas {
    private Statement  instruccionSql;
    private Connection conexion;
    private ResultSet  resultados;
    private String sql, mensaje;
    /**
    * Establece la conexión con la base de datos y la tabla Laboratorio.
    */
    public Empresas(){
        conexion = ConexionBD.conectarBD();
        if (conexion == null)
            System.exit(1000);
        else{
            try{
                instruccionSql = conexion.createStatement();
            }
            catch(SQLException error){
                mensaje = errorSQL(error.getSQLState());
                JOptionPane.showMessageDialog(null, mensaje);
            }
        } 
    }
    public final ResultSet sqlrRegistros(){
        try{
            sql = "Select e.*\n";
            sql+= "From empresa e\n";
            sql+= "Where e.empresa is not null\n";
            resultados = instruccionSql.executeQuery(sql);
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistros(String empresa, String empresaId){
        try{
            sql = "Select e.*\n";
            sql+= "From empresa e\n";
            sql+= "Where e.empresa is not null\n";
            if(!empresa.isEmpty()) {
                sql+= "and e.empresa = '" + empresa + "'\n";
            }
            if(!empresaId.isEmpty()) {
                sql+= "and e.empresa_id = '" + empresaId + "'\n";
            }
            System.out.println(sql);
            resultados = instruccionSql.executeQuery(sql);
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }

    public final ResultSet sqlrRegistrosBarTablero(){
        try{
            sql = "Select e.empresa\n";
            sql+= "  	 ,concat(count(n.casos_id),'.0') as nuevo\n";
            sql+= "  	 ,concat(count(a.casos_id),'.0') as asignado\n";
            sql+= "  	 ,concat(count(c.casos_id),'.0') as cerrado\n";
            sql+= "      ,DATE_FORMAT(now(),'%m/%Y') as fecha_filtro\n";
            sql+= "From empresa e\n";
            sql+= "Left Join usuario u on u.usuario_id = e.empresa_id\n";
            sql+= "Left Join casos n on n.usuario_solicitante = u.usuario_id and n.casos_estatus = 1 and DATE_FORMAT(n.casos_fecha_creado,'%m/%Y') = DATE_FORMAT(now(),'%m/%Y')\n";
            sql+= "Left Join casos a on a.usuario_solicitante = u.usuario_id and a.casos_estatus = 2 and DATE_FORMAT(a.casos_fecha_creado,'%m/%Y') = DATE_FORMAT(now(),'%m/%Y')\n";
            sql+= "Left Join casos c on c.usuario_solicitante = u.usuario_id and c.casos_estatus = 3 and DATE_FORMAT(c.casos_fecha_creado,'%m/%Y') = DATE_FORMAT(now(),'%m/%Y')\n";
            sql+= "Group By u.empresa_id;\n";
            resultados = instruccionSql.executeQuery(sql);
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }    
    /**
    * Cierra la conexion con la base de datos.
    */
    public void cerrarConexion(){
        try{  
            instruccionSql.close();
            conexion.close(); 
        }
	catch(SQLException error){
            JOptionPane.showMessageDialog(null,"Error en conexion. \n"+
                                          error);
            System.exit(200);
	}     
    }
}
