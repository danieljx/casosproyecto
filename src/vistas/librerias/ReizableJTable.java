/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.librerias;

import javax.swing.JTable;

/**
 *
 * @author Anthony
 */
public class ReizableJTable extends ZebraJTable {

    public ReizableJTable(javax.swing.table.TableModel dataModel) {
        super(dataModel);
        this.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
    }
    
    @Override
    public boolean getScrollableTracksViewportWidth() {
        return getPreferredSize().width < getParent().getWidth();
    }    
}
