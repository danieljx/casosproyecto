/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.reportes;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import modelos.Casos;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import vistas.componentes.Colores;

/**
 *
 * @author Anthony
 */
public class ReporteExcelCasosLista {
    private Casos casos;
    private ResultSet datos;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
    String strDate = sdfDate.format(new Date());
    private String rutaArchivo = System.getProperty("user.home")+"/reporteCasosLista" + strDate + ".xls";

    public ReporteExcelCasosLista(int estatus, int tipo, String cedula, String nombre, String fechaInicio, String fechaFin) throws IOException, SQLException {
        File archivoXLS = new File(rutaArchivo);

        if(archivoXLS.exists()) {
            archivoXLS.delete();
        }
        archivoXLS.createNewFile();
        casos = new Casos();
        datos = casos.sqlrRegistros(estatus, tipo, cedula, nombre, fechaInicio, fechaFin);
        
        Workbook libro = new HSSFWorkbook();
        FileOutputStream archivo = new FileOutputStream(archivoXLS);
        Sheet hoja = libro.createSheet("Lista de Casos");
        Row fila = hoja.createRow(0);
        imprimirTitulos(fila);
        int count =  1;
        while (datos.next()){
            fila = hoja.createRow(count);
            Cell celdaTexto = fila.createCell(0);
                 celdaTexto.setCellValue(datos.getString("casos_id"));
            Cell celdaTexto2 = fila.createCell(1);
                 celdaTexto2.setCellValue(Casos.getCasosEstatus(Integer.parseInt(datos.getString("casos_estatus"))));
            Cell celdaTexto3 = fila.createCell(2);
                 celdaTexto3.setCellValue(datos.getString("casos_estatus"));
            Cell celdaTexto4 = fila.createCell(3);
                 celdaTexto4.setCellValue(datos.getString("fecha_creado"));
            Cell celdaTexto5 = fila.createCell(4);
                 celdaTexto5.setCellValue(datos.getString("solicitante"));
            Cell celdaTexto6 = fila.createCell(5);
                 celdaTexto6.setCellValue(datos.getString("solicitante_cedula"));
            Cell celdaTexto7 = fila.createCell(6);
                 celdaTexto7.setCellValue(datos.getString("casos_descripcion"));
            Cell celdaTexto8 = fila.createCell(7);
                 celdaTexto8.setCellValue(datos.getString("tecnico"));
            Cell celdaTexto9 = fila.createCell(8);
                 celdaTexto9.setCellValue(datos.getString("fecha_cerrado"));
            count++;
        }
        libro.write(archivo);
        archivo.close();
        Desktop.getDesktop().open(archivoXLS);        
    }
    public void imprimirTitulos(Row fila) {
        Cell celda = fila.createCell(0);
             celda.setCellValue("# Caso");
        Cell celda2 = fila.createCell(1);
             celda2.setCellValue("Estatus");
        Cell celda3 = fila.createCell(2);
             celda3.setCellValue("Tipo");
        Cell celda4 = fila.createCell(3);
             celda4.setCellValue("F.Creado");
        Cell celda5 = fila.createCell(4);
             celda5.setCellValue("Solicitante");
        Cell celda6 = fila.createCell(5);
             celda6.setCellValue("Cedula");
        Cell celda7 = fila.createCell(6);
             celda7.setCellValue("Descripcion");
        Cell celda8 = fila.createCell(7);
             celda8.setCellValue("Tecnico");
        Cell celda9 = fila.createCell(8);
             celda9.setCellValue("F.Cerrado");
    }
    
}
