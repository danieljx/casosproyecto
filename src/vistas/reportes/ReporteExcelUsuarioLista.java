/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.reportes;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import modelos.Usuarios;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author Anthony
 */
public class ReporteExcelUsuarioLista {
    private Usuarios usuarios;
    private ResultSet datos;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
    String strDate = sdfDate.format(new Date());
    private String rutaArchivo = System.getProperty("user.home")+"/reporteUsuariosLista" + strDate + ".xls";

    public ReporteExcelUsuarioLista(String usuario, String cedula, String nombre, String empresa, String departamento) throws IOException, SQLException {
        File archivoXLS = new File(rutaArchivo);

        if(archivoXLS.exists()) {
            archivoXLS.delete();
        }
        archivoXLS.createNewFile();
        usuarios = new Usuarios();
        datos = usuarios.sqlrRegistros(usuario, cedula, nombre, empresa, departamento);
        
        Workbook libro = new HSSFWorkbook();
        FileOutputStream archivo = new FileOutputStream(archivoXLS);
        Sheet hoja = libro.createSheet("Lista de Usuarios");
        Row fila = hoja.createRow(0);
        imprimirTitulos(fila);
        int count =  1;
        while (datos.next()){
            fila = hoja.createRow(count);
            Cell celdaTexto = fila.createCell(0);
                 celdaTexto.setCellValue(datos.getString("usuario_cedula"));
            Cell celdaTexto2 = fila.createCell(1);
                 celdaTexto2.setCellValue(datos.getString("usuario_nombre") + " " + datos.getString("usuario_apellido"));
            Cell celdaTexto3 = fila.createCell(2);
                 celdaTexto3.setCellValue(datos.getString("empresa"));
            Cell celdaTexto4 = fila.createCell(3);
                 celdaTexto4.setCellValue(datos.getString("usuario_departamento"));
            Cell celdaTexto5 = fila.createCell(4);
                 celdaTexto5.setCellValue(datos.getString("usuario_extension"));
            count++;
        }
        libro.write(archivo);
        archivo.close();
        Desktop.getDesktop().open(archivoXLS);        
    }
    public void imprimirTitulos(Row fila) {
        Cell celda = fila.createCell(0);
             celda.setCellValue("Cédula");
        Cell celda2 = fila.createCell(1);
             celda2.setCellValue("Apellidos y Nombres");
        Cell celda3 = fila.createCell(2);
             celda3.setCellValue("Empresa");
        Cell celda4 = fila.createCell(3);
             celda4.setCellValue("Departamento");
        Cell celda5 = fila.createCell(4);
             celda5.setCellValue("Extension");
    }
    
}
