/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import vistas.componentes.Botones;
import vistas.componentes.CampoTexto;
import Componentes.ComboTabla;
import static SQL.MensajesDeError.errorSQL;
import controladores.AccionBotonFormulario;
import controladores.AccionCampoTexto;
import controladores.CerrarVentana;
import controladores.EscuchaCerrarSistema;
import controladores.EscuchaCancelar;
import controladores.EscuchaAceptar;
import controladores.EscuchaBuscar;
import controladores.EscuchaEstatus;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import modelos.Empresas;
import modelos.Casos;
import modelos.Sesion;
import modelos.Usuarios;
import vistas.componentes.Colores;

/**
 *
 * @author dvillanueva
 */
public class EditarCasos implements CerrarVentana, AccionBotonFormulario, AccionCampoTexto {
    private int casosId, casosStatus, casosSoli, casosTecn;
    private final JPanel panelForm, panelDatosBasicos, panelDatosDescrip, panelDatosObserv, panelBotonera, panel2;
    private final JLabel casoLabelId, casoLabelIdText, casoLabelFechaCreada, casoLabelFechaCreadaText,
                         casoLabelEstatus, casoLabelEstatusText, casoLabelSol,
                         casoLabelSolText, casoLabelTec, casoLabelTecText;
    private final ComboTabla tipoCombo, asigCombo;
    private CampoTexto campoAsignar;
    private JTextArea descrip, observ;
    private Casos caso;
    private ResultSet datos;
    private String mensaje;
    private boolean BoK = true;
    private JButton botonAsignar, botonCerrar;
    private String[] arrayBotones = {"Guardar","Cancelar"};
    private Color[] arrayBotonesColores = {Colores.verde2,Colores.gris3};
    
    private final Botones botonesAccion;
    private TablaCasos tabla;
    private final JButton botonBuscar;
    private JFrame frame;
    private JDialog modal;
    private final JPanel panel;
    private final CampoTexto campoSol;
    private final JButton botonBuscarSol;
    private final EditarCasos casosFrame;
    private Usuarios usuarios;
    private ResultSet datosUsuarios;

    public EditarCasos(int casosId, TablaCasos tabla) {
        this(casosId,tabla,null);
    }
    public EditarCasos(int casosId) {
        this(casosId,null);
    }
    public EditarCasos(int casosId, TablaCasos tabla, JFrame frame) {
        this.casosId = casosId;
        this.tabla = tabla;
        this.frame = frame;
        this.casosFrame = this;
        this.usuarios = new Usuarios();
        modal = new JDialog(frame, "Editar Casos", true);
        modal.setSize(860,550);
        modal.setResizable(false);
        modal.setIconImage(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/glyphicons-151-edit.png")).getImage());
        modal.addWindowListener(new EscuchaCerrarSistema(this));
        
        tipoCombo  = new ComboTabla("Tipo");
        tipoCombo.cargarCombo("0",Casos.getCasosTipos());
        Usuarios usuario  = new Usuarios();
        asigCombo  = new ComboTabla("Asignado");
        asigCombo.cargarCombo("0",usuario.sqlrRegistroDepartamento(2));
        

        campoSol = new CampoTexto("Solicitante", 15);
        botonBuscarSol = new JButton();
        botonBuscarSol.setIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-89-address-book.png")));
        botonBuscarSol.setBackground(Colores.azul2);
        botonBuscarSol.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                new BuscarTercero(modal,2,"!=",campoSol,casosFrame);
            }
            
        });
        campoSol.agregarBoton(botonBuscarSol);
        campoSol.agregarCampoColor(Colores.gris3);
        campoSol.hacerEditable(false);        
        
        campoAsignar = new CampoTexto("Asignar", 15);
        botonBuscar = new JButton();
        botonBuscar.setIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-89-address-book.png")));
        botonBuscar.setBackground(Colores.azul2);
        botonBuscar.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                new BuscarTercero(modal,2,"",campoAsignar,casosFrame);
            }
            
        });
        campoAsignar.agregarBoton(botonBuscar);
        campoAsignar.agregarBotonEscucha(new EscuchaBuscar(this));
        campoAsignar.agregarCampoColor(Colores.gris3);
        campoAsignar.hacerEditable(false);
                
        casoLabelId             = new JLabel("# Caso:");
        casoLabelIdText         = new JLabel();
        casoLabelFechaCreada    = new JLabel("Fecha Creada:");
        casoLabelFechaCreadaText= new JLabel();
        casoLabelEstatus        = new JLabel("Estatus:");
        casoLabelEstatusText    = new JLabel();
        casoLabelSol            = new JLabel("Solicitante:");
        casoLabelSolText        = new JLabel();
        casoLabelTec            = new JLabel("Tecnico");
        casoLabelTecText        = new JLabel();
        descrip                 = new JTextArea();
        observ                  = new JTextArea();
        cargarDatos();
        panelDatosBasicos = new JPanel();
        panelDatosBasicos.setLayout(null);
        panelDatosBasicos.setBorder(new TitledBorder("Datos Básicos"));
        panelDatosBasicos.add(casoLabelId);
        casoLabelId.setBounds(20, 20, 100, 25);
        panelDatosBasicos.add(casoLabelIdText);
        casoLabelIdText.setBounds(110, 20, 100, 25);
        panelDatosBasicos.add(casoLabelFechaCreada);
        casoLabelFechaCreada.setBounds(20, 50, 100, 25);
        panelDatosBasicos.add(casoLabelFechaCreadaText);
        casoLabelFechaCreadaText.setBounds(110, 50, 150, 25);
        panelDatosBasicos.add(casoLabelEstatus);
        casoLabelEstatus.setBounds(20, 80, 150, 25);
        panelDatosBasicos.add(casoLabelEstatusText);
        casoLabelEstatusText.setBounds(110, 80, 150, 25);
        panelDatosBasicos.add(casoLabelSol);
        casoLabelSol.setBounds(20, 110, 150, 25);
        panelDatosBasicos.add(casoLabelSolText);
        casoLabelSolText.setBounds(110, 110, 150, 25);
        if(casosStatus != 1) {
            panelDatosBasicos.add(casoLabelTec);
            casoLabelTec.setBounds(20, 140, 150, 25);
            panelDatosBasicos.add(casoLabelTecText);
            casoLabelTecText.setBounds(110, 140, 150, 25);
        } else {
            panelDatosBasicos.add(tipoCombo);
            tipoCombo.setBounds(20, 140, 280, 70);
            panelDatosBasicos.add(campoSol);
            campoSol.setBounds(40, 210, 280, 70);
            panelDatosBasicos.add(campoAsignar);
            campoAsignar.setBounds(40, 280, 280, 80);
        }
        panelDatosDescrip = new JPanel();
        panelDatosDescrip.setLayout(null);
        panelDatosDescrip.setBorder(new TitledBorder("Descripcion"));
        descrip.setLineWrap(true);
        descrip.setWrapStyleWord(true);
        descrip.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                char c = ke.getKeyChar();
                  if(descrip.getText().length() >= 240) {
                      ke.consume();
                  }
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        panelDatosDescrip.add(descrip);
        descrip.setBounds(20, 50, 260, 150);
        
        panelDatosObserv = new JPanel();
        panelDatosObserv.setLayout(null);
        panelDatosObserv.setBorder(new TitledBorder("Observacion"));
        observ.setLineWrap(true);
        observ.setWrapStyleWord(true);
        observ.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                char c = ke.getKeyChar();
                  if(descrip.getText().length() >= 240) {
                      ke.consume();
                  }
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        panelDatosObserv.add(observ);
        observ.setBounds(20, 50, 260, 150);
        
        panelForm = new JPanel();
        panelForm.setLayout(new GridLayout(1,2));
        panelForm.add(panelDatosBasicos);
        panel2 = new JPanel(new GridLayout(2,1));
        panel2.add(panelDatosDescrip);
        panel2.add(panelDatosObserv);
        panelForm.add(panel2);
        
        panelBotonera = new JPanel();
        botonesAccion = new Botones(arrayBotones, arrayBotonesColores);
        botonesAccion.asignarOyente(0, new EscuchaAceptar(this));
        botonesAccion.asignarOyente(1, new EscuchaCancelar(this));
        
        botonAsignar = new JButton("Asignar");
        botonAsignar.setBackground(Colores.amarillo2);
        botonAsignar.addActionListener(new EscuchaEstatus(2,this));
        
        botonCerrar = new JButton("Cerrar");
        botonCerrar.setBackground(Colores.rojo2);
        botonCerrar.addActionListener(new EscuchaEstatus(3,this));
        if(Sesion.getUsuarioDepartamento() == 1 || Sesion.getUsuarioDepartamento() == 2) {
            if(casosStatus == 1) {
                botonesAccion.agregarNuevoBoton(botonAsignar);
            } else if(casosStatus == 2) { 
                botonesAccion.agregarNuevoBoton(botonCerrar);
            }
        }
        panelBotonera.add(botonesAccion);

        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(panelForm,BorderLayout.CENTER);
        panel.add(panelBotonera,BorderLayout.SOUTH);
        modal.add(panel);
        //modal.pack();
        modal.setLocationRelativeTo(frame);
        modal.setVisible(true);
    }
    public boolean validarFormulario() {
        String texto = "";
        if(Sesion.getUsuarioDepartamento() == 1) {
            if(tipoCombo.obtenerId().equals("0")) {
                texto+="*Debe Seleccionar Tipo\n";
                BoK = false;
            }
            if(campoSol.obtenerContenido().trim().isEmpty()) {
                texto+="*Debe Seleccionar Solicitante\n";
                BoK = false;
            }
            if(campoAsignar.obtenerContenido().trim().isEmpty()) {
                texto+="*Debe Seleccionar Asignado\n";
                BoK = false;
            }
        } else if(Sesion.getUsuarioDepartamento() == 2){
            if(observ.getText().trim().isEmpty()) {
                texto+="*El Campo Observacion no puede Estar Vacio\n";
                BoK = false;
            }
        } else {
            if(descrip.getText().trim().isEmpty()) {
                texto+="*El Campo Descripcion no puede Estar Vacio\n";
                BoK = false;
            } else {
                BoK = true;
            }
        }
        if(!BoK) {
            JOptionPane.showMessageDialog(modal, texto);
        }
        return BoK;
    }
    public CampoTexto getCampoSol() {
        return campoSol;
    }
    public CampoTexto getCampoAsig() {
        return campoAsignar;
    }
    public void cargarDatos() {
        if(casosId != 0) {
            caso = new Casos();
            datos = caso.sqlrRegistroId(casosId);
            try {
                while(datos.next()) {
                    casoLabelIdText.setText(datos.getString("casos_id"));
                    casoLabelFechaCreadaText.setText(datos.getString("fecha_creado"));
                    casoLabelEstatusText.setText(Casos.getCasosEstatus(Integer.parseInt(datos.getString("casos_estatus"))));
                    casoLabelSolText.setText(datos.getString("solicitante"));
                    casoLabelTecText.setText(datos.getString("tecnico"));
                    descrip.setText(datos.getString("casos_descripcion"));
                    observ.setText(datos.getString("casos_observacion"));
                    tipoCombo.posicionarCombo(Integer.parseInt((datos.getString("casos_tipo")==null?"0":datos.getString("casos_tipo"))));
                    asigCombo.posicionarCombo(Integer.parseInt((datos.getString("usuario_tecnico")==null?"0":datos.getString("usuario_tecnico"))));
                    if(datos.getString("casos_estatus") == null) {
                        casosStatus   = 1;
                    } else {
                        casosStatus = Integer.parseInt(datos.getString("casos_estatus"));
                    }
                    if(datos.getString("usuario_solicitante") == null) {
                        casosSoli   = Sesion.getUsuarioId();
                    } else {
                        casosSoli   = Integer.parseInt(datos.getString("usuario_solicitante"));
                        datosUsuarios = usuarios.sqlrRegistroId(casosSoli);
                        if(datosUsuarios.next()) {
                            campoSol.cambiarContenido(datosUsuarios.getString("usuario_nombre") + " " +  datosUsuarios.getString("usuario_apellido"));
                        }
                    }
                    if(datos.getString("usuario_tecnico") == null) {
                        casosTecn = 1;
                    } else {
                        casosTecn   = Integer.parseInt(datos.getString("usuario_tecnico"));
                        datosUsuarios = usuarios.sqlrRegistroId(casosTecn);
                        if(datosUsuarios.next()) {
                            campoAsignar.cambiarContenido(datosUsuarios.getString("usuario_nombre") + " " +  datosUsuarios.getString("usuario_apellido"));
                        }
                    }
                }
            } catch (SQLException error) {
                mensaje = errorSQL(error.getSQLState());
                JOptionPane.showMessageDialog(null,mensaje);
                System.exit(200);
            }
        } else {
            casosStatus = 1;
            casosTecn   = 0;
            casosSoli   = Sesion.getUsuarioId();
            casoLabelSolText.setText(Sesion.getUsuarioNombre());
            casoLabelEstatusText.setText(Casos.getCasosEstatus(1));
        }
        descrip.setEditable(casosStatus != 3);
        observ.setEditable(casosStatus != 3);
    }

    @Override
    public void cerrarVentana() {
        modal.dispose();
    }

    @Override
    public void aceptarAccion() {
        //if(casosStatus != 3) {
            if(validarFormulario()) {
                if(casosId != 0) {
                    tabla.modificarFila(Integer.parseInt(tipoCombo.obtenerId()), Integer.parseInt(asigCombo.obtenerId()), casosStatus, 
                                        descrip.getText(), observ.getText(), 
                                        casosSoli, casosTecn);
                } else {
                    tabla.agregarFila(Integer.parseInt(tipoCombo.obtenerId()), Integer.parseInt(asigCombo.obtenerId()), casosStatus, 
                                        descrip.getText(), observ.getText(), 
                                        casosSoli, casosTecn);
                }
                cerrarVentana();
                //Casos data = new Casos();
                //tabla.cargarTabla(data.sqlrRegistros());
            }
        //}
    }

    @Override
    public void cancelarAccion() {
        cerrarVentana();
    }

    @Override
    public void estatusAccion(int estatus) {
        int opts = JOptionPane.showConfirmDialog(null, "Coonfirma " + (estatus==2?"Asignar":"Cerrar") + " el Caso?", "Aviso", JOptionPane.YES_NO_OPTION);
        if(opts == 0 && validarFormulario()) {
            casosStatus = estatus;
            aceptarAccion();
        }
    }

    @Override
    public void botonAccion() {
    }
    public void setAsignar(int id) {
        casosTecn = id;
    }
    public void setSolicitante(int id) {
        casosSoli = id;
    }
}
