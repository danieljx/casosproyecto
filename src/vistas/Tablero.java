/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import vistas.componentes.Colores;
import vistas.componentes.tablero.CasosEmpresasBarChart;
import vistas.componentes.tablero.CasosTabla;
import vistas.componentes.tablero.CasosPieChart;

/**
 *
 * @author dvillanueva
 */
public class Tablero extends JPanel {
    private JPanel panelAcceso, panelDatos;
    private JInternalFrame frameGraficoPie, frameGraficoAcend, frameListaCasosNuevos, frameListaCasosCerrados;
    private JButton casosBoton, usuariosBoton;
    private final Contenido contenido;
    private final JButton salirBoton;
    private final JButton actualizarBoton;
    private final CasosTabla tablaResuelto;
    private final CasosTabla tablaNuevo;
    private final CasosEmpresasBarChart casosEmpresasBarChart;
    private final CasosPieChart casosPieChart;
    private final Tablero tablero;
    public Tablero(final Contenido contenido) {
        this.setLayout(new BorderLayout());
        this.setBackground(Colores.blanco);
        this.contenido = contenido;
        this.tablero = this;
        panelAcceso = new JPanel();
        panelAcceso.setLayout(new FlowLayout(FlowLayout.LEFT,3,3));
        panelAcceso.setBorder(new TitledBorder("Accesos Directos"));
        panelAcceso.setBackground(Colores.blanco);
        casosBoton = new JButton();
        casosBoton.setText("Casos");
        casosBoton.setFont(new Font("Tahoma", 1, 14));
        casosBoton.setMargin(new Insets(8, 14, 8, 14));
        casosBoton.setForeground(Color.WHITE);
        casosBoton.setBackground(Colores.azul);
        casosBoton.setIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-40-notes.png")));
        casosBoton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contenido.abrirListaCasos();
            }
        
        });
        usuariosBoton = new JButton();
        usuariosBoton.setText("Usuarios");
        usuariosBoton.setFont(new Font("Tahoma", 1, 14));
        usuariosBoton.setMargin(new Insets(8, 14, 8, 14));
        usuariosBoton.setForeground(Color.WHITE);
        usuariosBoton.setBackground(Colores.amarillo2);
        usuariosBoton.setIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-526-user-key.png")));
        usuariosBoton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contenido.abrirListaUsuarios();
            }
        
        });
        salirBoton = new JButton();
        salirBoton.setText("Salir");
        salirBoton.setFont(new Font("Tahoma", 1, 14));
        salirBoton.setMargin(new Insets(11, 14, 11, 14));
        salirBoton.setForeground(Color.WHITE);
        salirBoton.setBackground(Colores.rojo2);
        salirBoton.setIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-388-log-out.png")));
        salirBoton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contenido.cerrarVentana();
            }
        
        });
        actualizarBoton = new JButton();
        actualizarBoton.setText("Actualizar");
        actualizarBoton.setFont(new Font("Tahoma", 1, 14));
        actualizarBoton.setForeground(Color.WHITE);
        actualizarBoton.setBackground(Colores.verde2);
        actualizarBoton.setIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-748-synchronization.png")));
        actualizarBoton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tablero.actualizarVentana();
            }
        
        });        
        
        panelAcceso.add(usuariosBoton);
        panelAcceso.add(casosBoton);
        panelAcceso.add(actualizarBoton);
        panelAcceso.add(salirBoton);
        
        panelDatos = new JPanel();
        panelDatos.setLayout(new GridLayout(2,2,10,10));
        panelDatos.setBorder(new TitledBorder("Datos"));
        panelDatos.setBackground(Colores.blanco);
        
        frameGraficoPie = new JInternalFrame();
        frameGraficoPie.setLayout(new BorderLayout());
        frameGraficoPie.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        frameGraficoPie.setIconifiable(true);
        frameGraficoPie.setTitle("Casos por Estatus");
        frameGraficoPie.setFrameIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/glyphicons-43-pie-chart.png")));
        casosPieChart = new CasosPieChart();
        frameGraficoPie.setContentPane(casosPieChart);
        frameGraficoPie.setVisible(true);
        
        frameListaCasosNuevos = new JInternalFrame();
        frameListaCasosNuevos.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        frameListaCasosNuevos.setIconifiable(true);
        frameListaCasosNuevos.setTitle("Lista Nuevos Casos");
        frameListaCasosNuevos.setFrameIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/glyphicons-132-inbox-plus.png")));
        tablaNuevo = new CasosTabla(1);
        frameListaCasosNuevos.setContentPane(tablaNuevo);
        frameListaCasosNuevos.setVisible(true);

        frameGraficoAcend = new JInternalFrame();
        frameGraficoAcend.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        frameGraficoAcend.setIconifiable(true);
        frameGraficoAcend.setTitle("Casos por Empresa");
        frameGraficoAcend.setFrameIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/glyphicons-42-charts.png")));
        casosEmpresasBarChart = new CasosEmpresasBarChart();
        frameGraficoAcend.setContentPane(casosEmpresasBarChart);
        frameGraficoAcend.setVisible(true);

        frameListaCasosCerrados = new JInternalFrame();
        frameListaCasosCerrados.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        frameListaCasosCerrados.setIconifiable(true);
        frameListaCasosCerrados.setTitle("Lista Casos Resueltos");
        frameListaCasosCerrados.setFrameIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/glyphicons-136-inbox-out.png")));
        tablaResuelto = new CasosTabla(3);
        frameListaCasosCerrados.setContentPane(tablaResuelto);
        frameListaCasosCerrados.setVisible(true);        
        
        panelDatos.add(frameGraficoPie);
        panelDatos.add(frameListaCasosNuevos);
        panelDatos.add(frameGraficoAcend);
        panelDatos.add(frameListaCasosCerrados);
        this.add(panelAcceso,BorderLayout.NORTH);
        this.add(panelDatos,BorderLayout.CENTER);
    }
    public void actualizarVentana() {
        casosPieChart.actualizar();
        tablaNuevo.recargarTabla();
        casosEmpresasBarChart.actualizar();
        tablaResuelto.recargarTabla();
    }
}
