/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import controladores.AccionBotonFormulario;
import controladores.CerrarVentana;
import controladores.EscuchaCancelar;
import controladores.EscuchaCerrarSistema;
import controladores.EscuchaAceptar;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import modelos.Usuarios;
import vistas.componentes.Colores;
import vistas.componentes.LoginPanel;
import vistas.componentes.LogoPanel;


/**
 *
 * @author dvillanueva
 */
public class Login extends JFrame implements CerrarVentana, AccionBotonFormulario{
    private final JPanel usuarioPanel, clavePanel, botonesPanel, principalPanel;
    private LogoPanel logoPanel;
    private final JLabel usuarioLabel, claveLabel;
    private final JTextField usuarioText;
    private final JPasswordField claveText;
    private final JButton enviarLog, cancelarLog;
    
    public Login() {
        usuarioPanel    = new JPanel();
        clavePanel      = new JPanel();
        botonesPanel    = new JPanel();
        principalPanel  = new JPanel();
        usuarioLabel    = new JLabel();
        claveLabel      = new JLabel();
        usuarioText     = new JTextField(10);
        claveText       = new JPasswordField(10);
        enviarLog       = new JButton();
        cancelarLog     = new JButton();
                
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Iniciar Sesión");
        this.setIconImage(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/vistas/imagenes/logo-icon.png")).getImage());
        this.addWindowListener(new EscuchaCerrarSistema(this));
        //this.setResizable(false);
        //this.setLocationRelativeTo(null);
        this.setContentPane(new JLabel(new ImageIcon(getClass().getResource("/vistas/imagenes/login-bg.jpg"))));
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setLayout(new BorderLayout());
        usuarioText.setText("system");
        claveText.setText("system");
        
        usuarioLabel.setFont(new Font("Tahoma", 0, 24));
        usuarioLabel.setForeground(new Color(99, 99, 99));
        usuarioLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        usuarioLabel.setText("Usuario:  ");
        
        usuarioPanel.setAutoscrolls(true);
        usuarioPanel.setLayout(null);
        usuarioPanel.setBackground(Colores.transparente);
        usuarioPanel.add(usuarioLabel);
        usuarioLabel.setBounds(50, 10, 250, 30);
        usuarioPanel.add(usuarioText);
        usuarioText.setBackground(new Color(250, 250, 250));
        usuarioText.setForeground(new Color(102, 102, 102));
        usuarioText.setBounds(300, 10, 150, 30);

        claveLabel.setFont(new Font("Tahoma", 0, 24));
        claveLabel.setForeground(new Color(99, 99, 99));
        claveLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        claveLabel.setText("Contraseña:  ");
        
        clavePanel.setLayout(null);
        clavePanel.setAutoscrolls(true);
        clavePanel.setBackground(new Color(255, 255, 255, 0));
        clavePanel.add(claveLabel);
        claveLabel.setBounds(50, 10, 250, 30);
        clavePanel.add(claveText);
        claveText.setBackground(new Color(250, 250, 250));
        claveText.setForeground(new Color(102, 102, 102));
        claveText.setBounds(300, 10, 150, 30);
        
        botonesPanel.setLayout(new FlowLayout());
        botonesPanel.setBackground(Colores.transparente);

        principalPanel.setLayout(new GridLayout(3,1));
        principalPanel.setBackground(Colores.transparente);

        enviarLog.setFont(new Font("Tahoma", 1, 20));
        enviarLog.setSize(300, 150);
        enviarLog.setText("Ingresar");
        enviarLog.setBackground(Colores.azul);
        enviarLog.setForeground(Color.WHITE);
        enviarLog.addActionListener(new EscuchaAceptar(this));
        //enviarLog.setIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/glyphicons/glyphicons-199-ok-circle.png")));
        //enviarLog.setIconTextGap(10);
        enviarLog.setFocusPainted(false);
        
        cancelarLog.setFont(new Font("Tahoma", 1, 20));
        cancelarLog.setText("Cancelar");
        cancelarLog.addActionListener(new EscuchaCancelar(this));
        cancelarLog.setIcon(new ImageIcon(getClass().getResource("/vistas/imagenes/glyphicons/glyphicons-198-remove-circle.png")));
        cancelarLog.setIconTextGap(10);
        cancelarLog.setFocusPainted(false);
        
        botonesPanel.add(enviarLog);
        //botonesPanel.add(cancelarLog);
        
        principalPanel.add(usuarioPanel);
        principalPanel.add(clavePanel);
        principalPanel.add(botonesPanel);
        
        
        //this.add(logoPanel);
        LoginPanel loginPanel = new LoginPanel();
        
        
        loginPanel.add(new LogoPanel(), BorderLayout.NORTH);
        loginPanel.add(principalPanel, BorderLayout.CENTER);
        
        
        Box box = new Box(BoxLayout.Y_AXIS);
        box.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        box.add(Box.createVerticalGlue());
        box.add(loginPanel);
        box.add(Box.createVerticalGlue());
        this.add(box);
        
    }
    
    public void cerrarVentana() {
        JOptionPane.showMessageDialog(this, "!! Hasta luego !!");
        System.exit(0);
    }

    @Override
    public void aceptarAccion() {
        Usuarios usuario = new Usuarios();
        String textoClave = new String(claveText.getPassword());
        if(usuarioText.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Ingrese Usuario");
        } else if(textoClave.equals("")) {
            JOptionPane.showMessageDialog(this, "Ingrese Contraseña");
        } else if(usuario.sqlrRegistroValidarLogin(this, usuarioText.getText(), textoClave)) {
            new Contenido(this).setVisible(true);
            usuarioText.setText("");
            claveText.setText("");
            this.setVisible(false);
        }
    }

    @Override
    public void cancelarAccion() {
        cerrarVentana();
    }

    @Override
    public void estatusAccion(int estatus) {
    }

  
}
