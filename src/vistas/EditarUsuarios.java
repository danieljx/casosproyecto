/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import vistas.componentes.Botones;
import Componentes.ComboTabla;
import static SQL.MensajesDeError.errorSQL;
import controladores.AccionBotonFormulario;
import controladores.AccionValidarCampos;
import controladores.CerrarVentana;
import controladores.EscuchaCerrarSistema;
import controladores.EscuchaCancelar;
import controladores.EscuchaAceptar;
import controladores.EscuchaValidarLetras;
import controladores.EscuchaValidarNumeros;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import modelos.Empresas;
import modelos.Usuarios;
import vistas.componentes.Colores;

/**
 *
 * @author dvillanueva
 */
public class EditarUsuarios implements CerrarVentana, AccionBotonFormulario, AccionValidarCampos {
    private final int usuarioId;
    private final JPanel panelForm, panelDatosBasicos, panelDatosEmpresa,panelError;
    private final JTextField usuarioTexto, usuarioCedulaTexto, usuarioExtTexto,
                             usuarioNombreTexto, usuarioApellidoTexto, usuarioEmailTexto,
                             usuarioClaveTexto, usuarioDepartTexto, usuarioSedeTexto;
    private final JLabel usuarioLabel, usuarioCedulaLabel, usuarioEmpresa, usuarioExtLabel,
                         usuarioNombreLabel, usuarioApellidoLabel, usuarioEmailLabel,
                         usuarioClaveLabel, usuarioDepartLabel, usuarioSedeLabel;
    private final ComboTabla empresaCombo;
    private Usuarios usuario;
    private ResultSet datos;
    private String mensaje;
    String[] arrayBotones = {"Guardar","Cancelar"};
    Color[] arrayBotonesColores = {Colores.verde2,Colores.gris3};
    private final JPanel panelFiltroBotonera;
    private final Botones botonesFiltro;
    private TablaUsuarios tabla;
    private final Empresas empresa;
    private final ComboTabla departUsuario;
    private JLabel Error;
    private boolean BoK = true;
    private Pattern Plantilla;
    private Matcher Resultado;
    private JFrame frame;
    private JDialog modal;
    private final JPanel panel;
    public EditarUsuarios(int usuarioId, TablaUsuarios tabla) {
        this(usuarioId,tabla,null);
    }
    public EditarUsuarios(int usuarioId) {
        this(usuarioId,null);
    }
    public EditarUsuarios(int usuarioId, TablaUsuarios tabla, JFrame frame) {
        this.usuarioId = usuarioId;
        this.frame = frame;
        this.tabla = tabla;
        modal = new JDialog(frame, "Editar Usuario", true);
        modal.setSize(600,400);
        modal.setResizable(false);
        modal.setIconImage(new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/glyphicons-151-edit.png")).getImage());
        modal.addWindowListener(new EscuchaCerrarSistema(this));
        usuarioTexto            = new JTextField();
        usuarioCedulaTexto      = new JTextField();
        usuarioNombreTexto      = new JTextField();
        usuarioNombreTexto.addKeyListener(new EscuchaValidarLetras(this));
        usuarioApellidoTexto    = new JTextField();
        usuarioApellidoTexto.addKeyListener(new EscuchaValidarLetras(this));
        usuarioEmailTexto       = new JTextField();
        usuarioClaveTexto       = new JTextField();
        usuarioDepartTexto      = new JTextField();
        usuarioSedeTexto        = new JTextField();
        usuarioExtTexto         = new JTextField();
        usuarioExtTexto.addKeyListener(new EscuchaValidarNumeros(this));
        empresaCombo            = new ComboTabla("Empresa");
        empresa = new Empresas();
        empresaCombo.cargarCombo("0",empresa.sqlrRegistros());
        
        usuarioLabel            = new JLabel("Usuario");
        usuarioCedulaLabel      = new JLabel("Cédula");
        usuarioEmpresa          = new JLabel("Empresa");
        usuarioNombreLabel      = new JLabel("Nombres");
        usuarioApellidoLabel    = new JLabel("Apellidos");
        usuarioEmailLabel       = new JLabel("E-Mail");
        usuarioClaveLabel       = new JLabel("Contraseña");
        usuarioDepartLabel      = new JLabel("Departamento");
        usuarioSedeLabel        = new JLabel("Sede");
        usuarioExtLabel         = new JLabel("Extension");

        panelDatosBasicos = new JPanel();
        panelDatosBasicos.setLayout(null);
        panelDatosBasicos.setBorder(new TitledBorder("Datos Básicos"));
        panelDatosBasicos.add(usuarioCedulaLabel);
        usuarioCedulaLabel.setBounds(20, 20, 100, 25);
        panelDatosBasicos.add(usuarioNombreLabel);
        usuarioNombreLabel.setBounds(20, 50, 100, 25);
        panelDatosBasicos.add(usuarioApellidoLabel);
        usuarioApellidoLabel.setBounds(20, 80, 100, 25);
        panelDatosBasicos.add(usuarioCedulaTexto);
        usuarioCedulaTexto.setBounds(100, 20, 150, 25);
        panelDatosBasicos.add(usuarioNombreTexto);
        usuarioNombreTexto.setBounds(100, 50, 150, 25);
        panelDatosBasicos.add(usuarioApellidoTexto);
        usuarioApellidoTexto.setBounds(100, 80, 150, 25);
        
        panelDatosEmpresa = new JPanel();
        panelDatosEmpresa.setLayout(null);
        panelDatosEmpresa.setBorder(new TitledBorder("Datos Sistema"));
        panelDatosEmpresa.add(usuarioLabel);
        usuarioLabel.setBounds(20, 20, 150, 25);
        panelDatosEmpresa.add(usuarioClaveLabel);
        usuarioClaveLabel.setBounds(20, 50, 150, 25);
        panelDatosEmpresa.add(usuarioEmailLabel);
        usuarioEmailLabel.setBounds(20, 80, 150, 25);
        
        panelDatosEmpresa.add(usuarioSedeLabel);
        usuarioSedeLabel.setBounds(20, 250, 150, 25);
        panelDatosEmpresa.add(usuarioExtLabel);
        usuarioExtLabel.setBounds(20, 280, 150, 25);
        
        panelDatosEmpresa.add(usuarioTexto);
        usuarioTexto.setBounds(100, 20, 150, 25);
        panelDatosEmpresa.add(usuarioClaveTexto);
        usuarioClaveTexto.setBounds(100, 50, 150, 25);
        panelDatosEmpresa.add(usuarioEmailTexto);
        usuarioEmailTexto.setBounds(100, 80, 150, 25);
        panelDatosEmpresa.add(empresaCombo);
        empresaCombo.setBounds(20, 110, 240, 70);
        
        departUsuario = new ComboTabla("Departamentos");
        departUsuario.cargarCombo("0",Usuarios.getUsuarioDepartamento());
        panelDatosEmpresa.add(departUsuario);
        departUsuario.setBounds(20, 180, 240, 70);
        
        
        panelDatosEmpresa.add(usuarioSedeTexto);
        usuarioSedeTexto.setBounds(120, 250, 130, 25);
        panelDatosEmpresa.add(usuarioExtTexto);
        usuarioExtTexto.setBounds(120, 280, 100, 25);
        
        panelForm = new JPanel();
        panelForm.setLayout(new GridLayout(1,2));
        panelForm.add(panelDatosBasicos);
        panelForm.add(panelDatosEmpresa);
                
        panelFiltroBotonera = new JPanel();
        panelFiltroBotonera.setLayout(new BorderLayout());
        Error = new JLabel("");
        Error.setHorizontalAlignment(SwingConstants.CENTER);
        Error.setForeground(Colores.rojo2);
        panelFiltroBotonera.add(Error,BorderLayout.NORTH);
        panelError = new JPanel();
        botonesFiltro = new Botones(arrayBotones, arrayBotonesColores);
        botonesFiltro.asignarOyente(0, new EscuchaAceptar(this));
        botonesFiltro.asignarOyente(1, new EscuchaCancelar(this));
        panelFiltroBotonera.add(botonesFiltro,BorderLayout.CENTER);

        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(panelForm,BorderLayout.CENTER);
        panel.add(panelFiltroBotonera,BorderLayout.SOUTH);
        modal.add(panel);
        //modal.pack();
        modal.setLocationRelativeTo(frame);
        cargarDatos();
        modal.setVisible(true);
    }
    public void cargarDatos() {
        if(usuarioId != 0) {
            usuario = new Usuarios();
            datos = usuario.sqlrRegistroId(usuarioId);
            try {
                while(datos.next()) {
                    usuarioTexto.setText(datos.getString("usuario"));
                    usuarioCedulaTexto.setText(datos.getString("usuario_cedula"));
                    usuarioNombreTexto.setText(datos.getString("usuario_nombre"));
                    usuarioApellidoTexto.setText(datos.getString("usuario_apellido"));
                    usuarioEmailTexto.setText(datos.getString("usuario_email"));
                    departUsuario.posicionarCombo(Integer.parseInt(datos.getString("usuario_departamento")));
                    usuarioSedeTexto.setText(datos.getString("usuario_sede"));
                    empresaCombo.posicionarCombo(Integer.parseInt(datos.getString("empresa_id")));
                }
            } catch (SQLException error) {
                mensaje = errorSQL(error.getSQLState());
                JOptionPane.showMessageDialog(null,mensaje);
                System.exit(200);
            }
        }
    }
    public boolean validarFormulario() {
        String texto = "";
        BoK = true;
        if(usuarioTexto.getText().trim().isEmpty()) {
            texto+="*El Campo del Usuario no puede estar vacio\n";
            BoK = false;
        }
        if(usuarioCedulaTexto.getText().trim().isEmpty()) {
            texto+="*El Campo Cedula no puede tener Numeros\n";
            BoK = false;
        }
        if(usuarioNombreTexto.getText().trim().isEmpty()) {
            texto+="*El Campo Nombre no puede estar vacio\n";
            BoK = false;
        }
        if(usuarioApellidoTexto.getText().trim().isEmpty()) {
            texto+="*El Campo Apellido no puede estar vacio\n";
            BoK = false;
        }
        if(usuarioEmailTexto.getText().trim().isEmpty()) {
            texto+="*El Campo Email no puede estar vacio\n";
            BoK = false;
        }
        if(usuarioSedeTexto.getText().trim().isEmpty()) {
            texto+="*El Campo Sede no puede estar vacio\n";
            BoK = false;
        }
        if(empresaCombo.obtenerId().equals("0")) {
            texto+="*Debe Seleccionar Empresa\n";
            BoK = false;
        }
        if(departUsuario.obtenerId().equals("0")) {
            texto+="*Debe Seleccionar Departamento\n";
            BoK = false;
        }
        Plantilla = Pattern.compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
        Resultado = Plantilla.matcher(usuarioEmailTexto.getText().trim());
        if(!Resultado.find()) {
            texto+="*Ingrese un Correo Valido\n";
            BoK = false;
        }
        if(!BoK) {
            JOptionPane.showMessageDialog(modal, texto);
        }
        return BoK;
    }
    @Override
    public void cerrarVentana() {
        modal.dispose();
    }
    
    @Override
    public void aceptarAccion() {
        if(validarFormulario()) {
            int usuarioExt = usuarioExtTexto.getText().isEmpty()?0:Integer.parseInt(usuarioExtTexto.getText());
            if(usuarioId != 0) {
                System.out.println(tabla);
                tabla.modificarFila(usuarioTexto.getText(), usuarioClaveTexto.getText(), 
                                    usuarioCedulaTexto.getText(), usuarioEmailTexto.getText(),  usuarioNombreTexto.getText(), 
                                    usuarioApellidoTexto.getText(), Integer.parseInt(empresaCombo.obtenerId()), empresaCombo.obtenerContenido(), usuarioSedeTexto.getText(), Integer.parseInt(departUsuario.obtenerId()), departUsuario.obtenerContenido(),
                                    usuarioExt);
            } else {
                tabla.agregarFila(usuarioTexto.getText(), usuarioClaveTexto.getText(), 
                                  usuarioCedulaTexto.getText(), usuarioEmailTexto.getText(),  usuarioNombreTexto.getText(), 
                                  usuarioApellidoTexto.getText(), Integer.parseInt(empresaCombo.obtenerId()), usuarioSedeTexto.getText(), Integer.parseInt(departUsuario.obtenerId()),
                                  usuarioExt);
            }
            cerrarVentana();
            tabla.cargarTabla(new Usuarios().sqlrRegistros());
        }
    }

    @Override
    public void cancelarAccion() {
        cerrarVentana();
    }

    @Override
    public void estatusAccion(int estatus) {
        
    }

    @Override
    public void letrasAccion(boolean resp) {
        if(!resp) {
            Error.setText("Ingresa Solo Letras");
        } else {
            Error.setText("");
        }        
        BoK = resp;
    }

    @Override
    public void numerosAccion(boolean resp) {
        if(!resp) {
            Error.setText("Ingresa Solo Numeros");
        } else {
            Error.setText("");
        }
        BoK = resp;
    }
}
