package Componentes;

import static SQL.MensajesDeError.errorSQL;
import java.awt.FlowLayout;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import vistas.componentes.Colores;

/**
 * Panel con borde que contiene un JComboBox en el cual se muestran todos los
 * registros de la tabla Laboratorio.
 * @author Hector Alvarez
 */
public class ComboTabla extends JPanel{
    private JComboBox combo;
    private JComboBox comboId;
    private JPanel panelIn = new JPanel();
       
    /**
     * Crea un panel con título en el borde, con un JComboBox dentro; El combo
     * tendra contenidos que están asociados a un Id: (Id - Contenido).
     * 
     * @param nombre titulo a colocar en el borde del panel
     */
    public ComboTabla(String nombre){
        setLayout(new FlowLayout());
        panelIn.setBackground(Colores.transparente);
        combo = new JComboBox();
        comboId = new JComboBox();
        panelIn.setLayout(new FlowLayout());
        panelIn.setBorder(BorderFactory.createTitledBorder(nombre));
        comboId.addItem("0");
        combo.addItem("Seleccione....");
        panelIn.add(combo);
        add(panelIn);
    }
    
    /**
     * Crea un panel sin título en el borde, con un JComboBox dentro.
     */
    public ComboTabla(){
        this(null);
    } 
    
    /**
     * Carga el combobox con los datos correspondientes (Id - Contenido).
     * @param id del elemento del combo que aparece seleccionado al inicio.
     * @return true si la carga se realizo correctamente, falso caso contrario.
     */
    public boolean cargarCombo(String id, ResultSet datos){
        boolean ok = true;
        try{
            if (datos != null)
                while (datos.next()){
                    comboId.addItem(datos.getString(1));
                    combo.addItem(datos.getString(2));
                }          
        }
        catch (SQLException e){
            String mensaje = errorSQL(e.getSQLState());
            JOptionPane.showMessageDialog(this, mensaje);
        }
        ubicarContenido(id);
        return ok;
    }


    public boolean cargarCombo(String id, String[] datos){
        boolean ok = true;
        if (datos != null && datos.length > 0) {
            for(int i=0; i < datos.length; i++) {
                comboId.addItem((i+1) + "");
                combo.addItem(datos[i]);
            }
        }
        ubicarContenido(id);
        return ok;
    }    
    /**
     * Devuelve un valor String con el contenido del elemento seleccionado en el
     * combo.
     * @return contenido del campo de texto.
     */
    public String obtenerContenido(){
        return combo.getSelectedItem().toString();
    }
    
    /**
     * Devuelve un valor int con la posicion del elemento seleccionado en el
     * combo.
     * @return contenido del campo de texto.
     */
    public int obtenerPosicionSeleccionada(){
        return combo.getSelectedIndex();
    }
    
    /**
     * Selecciona el elemento del combo que esta en la posicion indicada.
     * @param posicion indica la posicion del elemento que se quiere mostrar en
     * el combo.
     */
    public void posicionarCombo(int posicion){
        if (posicion >= 0 && posicion < combo.getItemCount())
            combo.setSelectedIndex(posicion);
    }
    /**
     * Devuelve un String con el Id asociado al elemento seleccionado en el
     * combo.
     * @return Id asociado al contenido del combo principal.
     */
    public String obtenerId(){
        int posicion = obtenerPosicionSeleccionada();
        comboId.setSelectedIndex(posicion);
        return comboId.getSelectedItem().toString();
    }
    
    /**
     * Selecciona el elemento del combo principal que corresponde a un Id dado.
     * @param id id del contenido a ubicar
     */
    public boolean ubicarContenido(String id){
        boolean encontrado = false;
        if (id != null){
            int cantElementos = comboId.getItemCount();
            for (int i = 0; i < cantElementos; i++){
                comboId.setSelectedIndex(i);
                if (id.equals(comboId.getSelectedItem())){
                    combo.setSelectedIndex(i);
                    encontrado = true;
                    break;
                }
            }
        }
        return encontrado;
    }
    
    public void asignarOyente(ItemListener oyente){
        combo.addItemListener(oyente);
    }
}