/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.componentes;

import java.awt.Color;

/**
 *
 * @author Anthony
 */
public interface Colores {
    Color blanco = new Color(236, 240, 241),
          azul = new Color(41, 128, 185), azul2 = new Color(52, 152, 219),
          azulOscuro = new Color(52, 73, 94), azulOscuro2 = new Color(44, 62, 80),
          rojo = new Color(192, 57, 43), rojo2 = new Color(231, 76, 60), 
          amarillo = new Color(241, 196, 15), amarillo2 = new Color(243, 156, 18),
          gris = new Color(189, 195, 199), gris2 = new Color(149, 165, 166), gris3 = new Color(127, 140, 141),
          agua = new Color(26, 188, 156), agua2 = new Color(22, 160, 133),
          verde = new Color(46, 204, 113), verde2 = new Color(39, 174, 96),
          morado = new Color(142, 68, 173), morado2 = new Color(155, 89, 182),
          transparente = new Color(255, 255, 255, 0), transparenteMedio = new Color(255, 255, 255, 123);
}
