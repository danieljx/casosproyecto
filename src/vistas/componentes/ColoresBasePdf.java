/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.componentes;

import com.itextpdf.text.BaseColor;

/**
 *
 * @author Anthony
 */
public interface ColoresBasePdf{
    BaseColor blanco = new BaseColor(236, 240, 241),
          azul = new BaseColor(41, 128, 185), azul2 = new BaseColor(52, 152, 219),
          azulOscuro = new BaseColor(52, 73, 94), azulOscuro2 = new BaseColor(44, 62, 80),
          rojo = new BaseColor(192, 57, 43), rojo2 = new BaseColor(231, 76, 60), 
          amarillo = new BaseColor(241, 196, 15), amarillo2 = new BaseColor(243, 156, 18),
          gris = new BaseColor(189, 195, 199), gris2 = new BaseColor(149, 165, 166), gris3 = new BaseColor(127, 140, 141),
          agua = new BaseColor(26, 188, 156), agua2 = new BaseColor(22, 160, 133),
          verde = new BaseColor(46, 204, 113), verde2 = new BaseColor(39, 174, 96),
          morado = new BaseColor(142, 68, 173), morado2 = new BaseColor(155, 89, 182),
          transparente = new BaseColor(255, 255, 255, 0), transparenteMedio = new BaseColor(255, 255, 255, 123);
}
