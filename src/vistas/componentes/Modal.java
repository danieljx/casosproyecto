/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.componentes;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Image;
import java.awt.Window;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author usuario
 */
public class Modal {
    private JDialog modal;
    public Modal(JFrame frame) {
        this(frame,"");
    }
    public Modal(JFrame frame, String title) {
        modal = new JDialog(frame, title, true);
        modal.setLayout(new BorderLayout());
        modal.pack();
        modal.setLocationRelativeTo(frame);
        modal.setVisible(true);
    }
    public void cambiarIcono(Image icono) {
        modal.setIconImage(icono);
    }
    public void agregarCabecera(JPanel cabecera) {
        modal.getContentPane().add(cabecera, BorderLayout.NORTH);
    }
    public void agregarContenido(JPanel contenido) {
        modal.getContentPane().add(contenido, BorderLayout.CENTER);
    }
    public void agregarBotonera(Botones botones) {
        modal.getContentPane().add(botones, BorderLayout.SOUTH);
    }
    
}
