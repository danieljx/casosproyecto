/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.componentes.tablero;

import static SQL.MensajesDeError.errorSQL;
import java.awt.BorderLayout;
import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import modelos.Casos;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import vistas.componentes.Colores;

/**
 *
 * @author Anthony
 */
public class CasosPieChart extends JPanel {
    private ChartPanel panel;
    private String fecha = "";
    private JFreeChart chart;
    public CasosPieChart() {
        this.setLayout(new BorderLayout());
        this.setBackground(Colores.blanco);
        chart = crearChart(cargarDatos());
        chart.setBackgroundPaint(Colores.blanco);
        panel = new ChartPanel(chart);
        panel.setBackground(Colores.blanco);
        this.add(new ChartPanel(chart));
    }
    public void actualizar() {
        this.removeAll();
        chart = crearChart(cargarDatos());
        chart.setBackgroundPaint(Colores.blanco);
        panel = new ChartPanel(chart);
        panel.setBackground(Colores.blanco);
        this.add(new ChartPanel(chart));
    }
    private PieDataset cargarDatos() {
        double nuevoData = 0.0, asignadoData = 0.0, cerradoData = 0.0;
        Casos casos = new Casos();
        ResultSet datos = casos.sqlrRegistrosPie();
        try {
            while(datos.next()){
               if(Integer.parseInt(datos.getString("casos_estatus")) == 1) {
                   nuevoData++;
               } else if(Integer.parseInt(datos.getString("casos_estatus")) == 2) {
                   asignadoData++;
               } else if(Integer.parseInt(datos.getString("casos_estatus")) == 3) {
                   cerradoData++;
               }
               fecha = datos.getString("fecha_filtro");
            }
        }
        catch(SQLException e){
            String mensaje = errorSQL(e.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(2000);
        }
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("Nuevos", new Double(nuevoData));
        dataset.setValue("Asignados", new Double(asignadoData));
        dataset.setValue("Cerrados", new Double(cerradoData));
        return dataset;        
    }
    private JFreeChart crearChart(PieDataset dataset) {
        JFreeChart chart = ChartFactory.createPieChart("Casos por Estatus: " + fecha,dataset,true,true,false);
        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
        plot.setNoDataMessage("No data available");
        plot.setCircular(false);
        plot.setLabelGap(0.02);
        plot.setSectionPaint("Nuevos", Colores.azul2);
        plot.setSectionPaint("Asignados", Colores.amarillo2);   
        plot.setSectionPaint("Cerrados", Colores.verde2);
        plot.setBackgroundPaint(Colores.blanco);
        plot.setOutlineVisible(false);
        return chart;
    }
    
}
