/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.componentes.tablero;

import Componentes.ModeloDeTabla;
import static SQL.MensajesDeError.errorSQL;
import controladores.EscuchaTablaItem;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import modelos.Casos;
import vistas.componentes.Colores;
import vistas.librerias.ReizableJTable;

/**
 *
 * @author Anthony
 */
public class CasosTabla extends JPanel {
    private ModeloDeTabla tablaModelo;
    String[] nombreColumnas  = {"# Caso","Estatus","Tipo","F.Creado","Solicitante","Cedula"};
    Object [] claseColumnas  = {0, "", "", "", "", ""};
    public JTable tabla;
    private TableColumn colId, colTipo, colNombre, colFecha, colCedula, colEstatus;
    private final Object[] datos = new Object[6];
    private JTextField campo;
    private final Casos casos;
    private ResultSet resultado;
    private final int estatus;
    
    public CasosTabla(int estatus) {
        this.estatus = estatus;
        this.setLayout(new BorderLayout());
        TitledBorder bordrer = BorderFactory.createTitledBorder("Listado");
                     bordrer.setTitleColor(Color.WHITE);
        this.setBorder(bordrer);
        this.setBackground(Colores.azulOscuro2);
        crearTabla();
        casos = new Casos();
        cargarTabla(casos.sqlrRegistros(estatus, 0, "", "", "", ""));
    }
    public void recargarTabla() {
        cargarTabla(casos.sqlrRegistros(estatus, 0, "", "", "", ""));
    }
    private final void crearTabla(){
        
        tablaModelo = new ModeloDeTabla(nombreColumnas, claseColumnas);
        campo = new JTextField();
        campo.setEditable(false);
        campo.setHorizontalAlignment(SwingConstants.CENTER);
        tabla = new ReizableJTable(tablaModelo) {

            private static final long serialVersionUID = 1L;
            private Border outside = new MatteBorder(1, 0, 1, 0, Color.red);
            private Border inside = new EmptyBorder(0, 1, 0, 1);
            private Border highlight = new CompoundBorder(outside, inside);

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component comp = super.prepareRenderer(renderer, row, column);
                JComponent jc = (JComponent) comp;
                comp.setForeground(Color.black);
                if(column == 1) {
                    int modelRow = convertRowIndexToModel(row);
                    String type = (String) getModel().getValueAt(modelRow, column);
                    Color  colorEstatus;
                    switch (type) {
                        case "Nuevo":
                            colorEstatus = (row % 2 == 0 ? Colores.azul2 : Colores.azul);
                            break;
                        case "Asignado":
                            colorEstatus = (row % 2 == 0 ? Colores.amarillo : Colores.amarillo2);
                            break;
                        case "Cerrado":
                            colorEstatus = (row % 2 == 0 ? Colores.verde : Colores.verde2);
                            break;
                        default: 
                            colorEstatus = (row % 2 == 0 ? Colores.azul2 : Colores.azul);
                            break;
                    }
                    comp.setForeground(Color.WHITE);
                    comp.setBackground(colorEstatus);
                    comp.setFont(new Font("Tahoma", Font.BOLD, 12));
                } else {
                    comp.setFont(tabla.getFont());
                }
                jc.setBorder(BorderFactory.createCompoundBorder(jc.getBorder(), BorderFactory.createEmptyBorder(0, 0, 0, 5)));
                return comp;
            }
        };
        tabla.setFillsViewportHeight(true);
        //tabla.addMouseListener(new EscuchaTablaItem(this));
        colId           = tabla.getColumnModel().getColumn(0);
        colEstatus      = tabla.getColumnModel().getColumn(1);
        colTipo         = tabla.getColumnModel().getColumn(2);
        colFecha        = tabla.getColumnModel().getColumn(3);
        colNombre       = tabla.getColumnModel().getColumn(4);
        colCedula       = tabla.getColumnModel().getColumn(5);
                
        colId.setMinWidth(50);
        colId.setMaxWidth(50);
        
        //colTipo.setMinWidth(150);
        //colTipo.setMaxWidth(150);
                
        colFecha.setMinWidth(150);
        colFecha.setMaxWidth(150);
                
        //colNombre.setMinWidth(150);
        //colNombre.setMaxWidth(150);
                
        colCedula.setMinWidth(120);
        colCedula.setMaxWidth(120);
                
        //colDescrip.setMinWidth(220);
        //colDescrip.setMaxWidth(220);
                
        colEstatus.setMinWidth(100);
        colEstatus.setMaxWidth(100);
                
        //colTec.setMinWidth(150);
        //colTec.setMaxWidth(150);  
        
        
        DefaultCellEditor ce = new DefaultCellEditor(campo);
        ce.setClickCountToStart(100);
        colId.setCellEditor(ce);
        colTipo.setCellEditor(ce);
        colFecha.setCellEditor(ce);
        colNombre.setCellEditor(ce);
        colCedula.setCellEditor(ce);
        colEstatus.setCellEditor(ce);
        
        JScrollPane scrollPanel = new JScrollPane(tabla);
        add(scrollPanel);
    }
    
    /**
     * Carga la JTable con los datos que se pasaran como paramentros a traves de
     * un ResultSet.
     * @param data ResultSet contentivo de los datos a cargar en la tabla.
     * @return Verdadero si la carga fue exitosa, falso en caso contrario.
     */
    public boolean cargarTabla(ResultSet data){
        boolean todoBien = false;
        int filas = tabla.getRowCount();
        /**
         * El siguiente for borra los registros previamente cargados.
         */
        if(filas > 0) {
            for (int i = filas - 1; i >= 0; i--) {
                tablaModelo.removeRow(i);
            }
        }
        try{
            while(data.next()){
               datos[0] = Integer.parseInt(data.getString("casos_id"));
               datos[1] = Casos.getCasosEstatus(Integer.parseInt(data.getString("casos_estatus")));
               datos[2] = Casos.getCasosTipos(Integer.parseInt(data.getString("casos_tipo")));
               datos[3] = data.getString("fecha_creado");
               datos[4] = data.getString("solicitante");
               datos[5] = data.getString("solicitante_cedula");
               tablaModelo.addRow(datos);
            }
           todoBien = true;
        }
        catch(SQLException e){
            String mensaje = errorSQL(e.getSQLState());
            JOptionPane.showMessageDialog(this,mensaje);
            System.exit(2000);
        }
       return todoBien; 
    }    
}
