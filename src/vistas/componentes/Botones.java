package vistas.componentes;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Botonera
 * @author Hector Alvarez
 */
public class Botones extends JPanel{
    JButton[] botones;
    Color colorBase = Colores.azul;
    ImageIcon[] iconos;
    Color[] colores;
    JPanel panelIn;

    public Botones(String[] nomBoton, Color[] colores, ImageIcon[] iconos){
        this(nomBoton, colores);
        this.iconos = iconos;
        if(iconos.length > 0) {
            for (int i = 0; i < nomBoton.length; i++){
                botones[i].setIcon(iconos[i]);
            }
        }
    }

    public Botones(String[] nomBoton, Color[] colores){
        this(nomBoton);
        this.colores = colores;
        if(colores.length > 0) {
            for (int i = 0; i < nomBoton.length; i++){
                botones[i].setBackground(colores[i]);
            }
        }
    }
    
    /**
     * Crea una botonera con tantos botones com indique el parametro.
     */
    public Botones(String[] nomBoton){
        setLayout(new FlowLayout());
        this.setBackground(Colores.transparente);
        botones = new JButton[nomBoton.length];
        panelIn = new JPanel();
        panelIn.setBackground(Colores.transparente);
        panelIn.setLayout(new FlowLayout());
        panelIn.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
        
        for (int i = 0; i < nomBoton.length; i++){
            botones[i] = new JButton(nomBoton[i]);
            botones[i].setFont(new Font("Tahoma", 1, 14));
            botones[i].setForeground(Color.WHITE);
            botones[i].setBackground(colorBase);
            panelIn.add(botones[i]);
        }
        add(panelIn);
    }
    public void agregarNuevoBoton(JButton nuevoB) {
        nuevoB.setFont(new Font("Tahoma", 1, 14));
        nuevoB.setForeground(Color.WHITE);
        panelIn.add(nuevoB);
    }
    public void asignarOyente(int numBoton, ActionListener oyente){
        if (numBoton >= 0 && numBoton < botones.length)
            botones[numBoton].addActionListener(oyente);
    }
}