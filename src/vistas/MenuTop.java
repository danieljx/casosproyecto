/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import controladores.CerrarVentana;
import controladores.EscuchaCasoLista;
import controladores.EscuchaCerrarSistema;
import controladores.EscuchaSalir;
import controladores.EscuchaUsuarioLista;
import java.awt.Font;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import modelos.Sesion;

/**
 *
 * @author dvillanueva
 */
public class MenuTop extends JMenuBar {
    JMenu archivo, usuarios, casos, salir;
    JMenuItem nuevoUsuario, nuevoCaso, proyecto, listaUsuario, listaCaso, equipo;
    JLabel labelUsuario;
    Font usuarioFont;
    Contenido contenido;
    public MenuTop(Contenido contenido) {
        this.contenido = contenido;
        archivo = new JMenu("Archivo");
        proyecto = new JMenuItem("Proyecto");
        archivo.add(proyecto);
        equipo = new JMenuItem("Integrantes");
        archivo.add(equipo);
        this.add(archivo);
        
        usuarios = new JMenu("Usuarios");
        nuevoUsuario = new JMenuItem("Nuevo Usuario");
        //usuarios.add(nuevoUsuario);
        listaUsuario = new JMenuItem("Lista de Usuarios");
        listaUsuario.addActionListener(new EscuchaUsuarioLista(contenido));
        usuarios.add(listaUsuario);
        if(Sesion.getUsuarioDepartamento() == 2 || Sesion.getUsuarioDepartamento() == 1 || Sesion.getUsuarioId() == 1){
            this.add(usuarios);
        }
        casos = new JMenu("Casos");
        nuevoCaso = new JMenuItem("Nuevo Caso");
        //casos.add(nuevoCaso);
        listaCaso = new JMenuItem("Lista de Casos");
        listaCaso.addActionListener(new EscuchaCasoLista(contenido));
        casos.add(listaCaso);
        this.add(casos);

        salir = new JMenu();
        salir.setText("Salir");
        salir.addMouseListener(new EscuchaSalir(contenido));
        this.add(salir);
        
        this.add(Box.createHorizontalGlue());
        labelUsuario = new JLabel(Sesion.getUsuarioNombre() + "   ",SwingConstants.RIGHT);
        usuarioFont  = new Font(labelUsuario.getFont().getName(),Font.ITALIC,labelUsuario.getFont().getSize());
        labelUsuario.setFont(usuarioFont);
        this.add(labelUsuario);
    }    
}
