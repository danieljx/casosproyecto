/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import controladores.CerrarVentana;
import controladores.EscuchaCerrarSistema;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import vistas.componentes.Colores;

/**
 *
 * @author dvillanueva
 */
public class Contenido extends JFrame implements CerrarVentana{
    private final JTabbedPane cuerpoTabs;
    private int tabListaUsuario,tabListaCasos;
    private ListaUsuarios listaUsuarios;
    private ListaCasos listaCasos;
    private final Login login;
    public Contenido(Login login) {
        this.login = login;
        //this.setResizable(false);
        this.setBackground(Colores.blanco);
        this.setIconImage(new ImageIcon(getClass().getResource("/vistas/imagenes/logo-icon.png")).getImage());
        this.addWindowListener(new EscuchaCerrarSistema(this));
        this.setJMenuBar(new MenuTop(this));
        tabListaUsuario = 0;
        tabListaCasos   = 0;
        cuerpoTabs = new JTabbedPane();
        cuerpoTabs.setBackground(Colores.blanco);
        cuerpoTabs.addTab("Tablero", new Tablero(this));
        this.setContentPane(new JLabel(new ImageIcon(getClass().getResource("/vistas/imagenes/login-bg.jpg"))));
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setLayout(new BorderLayout());
        this.add(cuerpoTabs);
    }
    public void abrirListaUsuarios() {
        if(tabListaUsuario == 0) {
            listaUsuarios = new ListaUsuarios(this);
            cuerpoTabs.addTab("Lista de Usuarios", listaUsuarios);
            tabListaUsuario = cuerpoTabs.indexOfComponent(listaUsuarios);
            cuerpoTabs.setSelectedIndex(tabListaUsuario);
        } else {
            cuerpoTabs.setSelectedIndex(tabListaUsuario);
        } 
    }
    public void abrirListaCasos() {
        if(tabListaCasos == 0) {
            listaCasos = new ListaCasos(this);
            cuerpoTabs.addTab("Lista de Casos", listaCasos);
            tabListaCasos = cuerpoTabs.indexOfComponent(listaCasos);
            cuerpoTabs.setSelectedIndex(tabListaCasos);
        } else {
            cuerpoTabs.setSelectedIndex(tabListaCasos);
        } 
    }
    
    public void cerrarVentana() {
        int opts = JOptionPane.showConfirmDialog(null, "Confirmar cerrar Sesion?", "Aviso", JOptionPane.YES_NO_OPTION);
        if(opts == 0) {
            this.dispose();
            this.login.setVisible(true);
        }
    }
}
