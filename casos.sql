-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-03-2016 a las 00:00:02
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.5.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `casos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `casos`
--

CREATE TABLE `casos` (
  `casos_id` int(11) NOT NULL,
  `casos_tipo` int(11) DEFAULT NULL,
  `casos_estatus` int(11) DEFAULT NULL,
  `casos_descripcion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `casos_observacion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `casos_autorizacion` int(11) DEFAULT NULL,
  `usuario_solicitante` int(11) DEFAULT NULL,
  `usuario_autorizador` int(11) DEFAULT NULL,
  `usuario_tecnico` int(11) DEFAULT NULL,
  `casos_fecha_creado` datetime DEFAULT NULL,
  `casos_fecha_asignado` datetime DEFAULT NULL,
  `casos_fecha_cerrado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `casos`
--

INSERT INTO `casos` (`casos_id`, `casos_tipo`, `casos_estatus`, `casos_descripcion`, `casos_observacion`, `casos_autorizacion`, `usuario_solicitante`, `usuario_autorizador`, `usuario_tecnico`, `casos_fecha_creado`, `casos_fecha_asignado`, `casos_fecha_cerrado`) VALUES
(1, 3, 3, 'Problemas Con mi computadora', 'Problemas Mentales del Usuario', NULL, 4, NULL, 2, '2015-11-23 01:02:36', '2015-11-23 02:02:36', '2015-11-23 03:02:36'),
(2, 2, 2, 'Problemas Con mi Internet', 'Revisando Problemas', NULL, 5, NULL, 3, '2015-11-23 12:02:36', '2015-11-23 04:02:36', '2015-11-23 05:02:36'),
(3, 3, 1, 'Problemas Con mi Clave de Usuario', '', NULL, 4, NULL, 8, '2015-11-24 12:02:36', NULL, NULL),
(7, 2, 1, 'asdasdadasdasdad', 'ghffgfgfgfgfgfg', 0, 2, 1, 1, '2016-03-10 16:40:27', NULL, NULL),
(8, 2, 3, 'asdasdzczxczxczxczxczxcczxc', '', 0, 2, 1, 1, '2016-03-10 16:44:22', '2016-03-14 16:39:43', '2016-03-14 16:45:24'),
(9, 5, 2, 'dsdfsdfsdfsdfyjgufgkuhll', '', 0, 4, 1, 6, '2016-03-10 16:48:56', '2016-03-14 16:39:34', NULL),
(10, 2, 1, 'kjdskjdksdjkskkd', 'sdfjnsnkwnkwdknssdsdsd', 0, 5, 1, 1, '2016-03-14 17:24:20', NULL, NULL),
(11, 3, 1, 'hhfyfgt		', 'hgfyfgtdgd', 0, 5, 1, 1, '2016-03-14 17:29:30', NULL, NULL),
(12, 3, 1, 'hhfyfgt		', 'hgfyfgtdgd', 0, 5, 1, 1, '2016-03-14 17:31:47', NULL, NULL),
(13, 2, 1, 'wsdfdsfdfdfsffsdf', 'sdfsdfsdfsdfsdfsdfsdfsdfs', 0, 3, 1, 1, '2016-03-14 18:19:04', NULL, NULL),
(14, 3, 1, 'sdssdsssdsdsdsds', 'sfdfddsdssdsfd', 0, 5, 1, 1, '2016-03-14 18:23:27', NULL, NULL),
(15, 3, 1, 'efxsdsdsd', 'sdsdsdsdsd', NULL, 4, 1, 1, '2016-03-14 18:27:01', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `empresa_id` int(11) NOT NULL,
  `empresa` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `empresa_rif` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `empresa_sede` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `empresa_departamento` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`empresa_id`, `empresa`, `empresa_rif`, `empresa_sede`, `empresa_departamento`) VALUES
(1, 'Super Autos Carabobo', 'J-30471998-1', 'Sede Principal', 'Administracion'),
(2, 'Super Autos Premium', 'J-29479394-0', 'Sede Principal', 'Administracion'),
(3, 'Super Autos Carabobo Camiones', 'J-29479385-1', 'Sede Principal', 'Administracion'),
(4, 'Super Autos Carabobo Los Llanos', 'J-31688327-2', 'Sede Principal', 'Administracion'),
(5, 'Mayor de Repuestos Tamaya', 'J-13470336-1', 'Sede Principal', 'Administracion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL,
  `usuario` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usuario_contrasena` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usuario_cedula` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usuario_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usuario_nombre` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usuario_apellido` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `empresa_id` int(11) DEFAULT NULL,
  `usuario_sede` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usuario_departamento` int(11) NOT NULL,
  `usuario_extension` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `usuario`, `usuario_contrasena`, `usuario_cedula`, `usuario_email`, `usuario_nombre`, `usuario_apellido`, `empresa_id`, `usuario_sede`, `usuario_departamento`, `usuario_extension`) VALUES
(1, 'system', 'system', '00000000', 'system@system.com', 'Administrador', 'Sistema', 0, '', 1, 0),
(2, 'kbarrios', '123', '24295163', 'kris.barrios@superautos.com.ve', 'Kris Kary', 'Barrios Barrios', 2, 'Principal', 2, 0),
(3, 'jrivero', '123', '888888', 'jean.rivero@superautos.com.ve', 'Jean', 'Rivero', 1, 'Latoneria', 2, 524),
(4, 'usuario1', '123', '5555555', 'usuario1@superautos.com.ve', 'Usuario', 'Uno', 2, 'Latoneria', 3, 669),
(5, 'usuario2', '123', '6666666', 'usuario2@superautos.com.ve', 'Usuario', 'Dos', 4, 'Latoneria', 3, 0),
(6, 'dvillanueva', '123qwe', '19365771', 'dvillanueva@superautos.com.ve', 'Daniel', 'Villanueva', 5, 'Principal', 1, 0),
(7, 'aespinal', '123', '2342342', 'aespinal@hotmail.com', 'Anthony', 'Espinal', 1, 'qweqweqweqw', 3, 2333),
(8, 'mquerrales', '123', '2342352554', 'mquerrales@hotmail.com', 'Monica', 'Querrales', 5, 'erwerwr', 3, 32);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `casos`
--
ALTER TABLE `casos`
  ADD PRIMARY KEY (`casos_id`),
  ADD KEY `caso_usuario_solicitante` (`usuario_solicitante`),
  ADD KEY `caso_usuario_autorizador` (`usuario_autorizador`),
  ADD KEY `caso_usuario_tecnico` (`usuario_tecnico`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`empresa_id`),
  ADD KEY `empresa_rif_codigo` (`empresa_rif`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`),
  ADD KEY `usuario_cedula_codigo` (`usuario_cedula`),
  ADD KEY `usuario_empresa_id` (`empresa_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `casos`
--
ALTER TABLE `casos`
  MODIFY `casos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `empresa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `casos`
--
ALTER TABLE `casos`
  ADD CONSTRAINT `caso_usuario_autorizador` FOREIGN KEY (`usuario_autorizador`) REFERENCES `usuario` (`usuario_id`),
  ADD CONSTRAINT `caso_usuario_solicitante` FOREIGN KEY (`usuario_solicitante`) REFERENCES `usuario` (`usuario_id`),
  ADD CONSTRAINT `caso_usuario_tecnico` FOREIGN KEY (`usuario_tecnico`) REFERENCES `usuario` (`usuario_id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`empresa_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
